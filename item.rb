# frozen_string_literal: true

class Item
  attr_accessor :price, :quantity, :name

  def initialize(params)
    @name = params[:name].to_s
    @price = params[:price].to_i
    @quantity = params[:quantity].to_i
  end

  def update(params)
    @name = params[:name] if params.key?(:name)
    @price = params[:price] if params.key?(:price)
    @quantity = params[:quantity] if params.key?(:quantity)
  end

  def self.from_file(path)
    raise(NotImplementedError.new, path)
  end

  def item_type
    'Товар'
  end

  def to_s
    "#{item_type} #{@name} стоит #{@price} руб. Остаток: #{@quantity} шт."
  end
end
