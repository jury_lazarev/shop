# frozen_string_literal: true

require_relative 'item'

class Film < Item
  attr_accessor :year, :director

  def initialize(params)
    super
    @year = params[:year].to_s
    @director = params[:director].to_s
  end

  def update(params)
    super
    @year = params[:year] if params.key?(:year)
    @director = params[:director] if params.key?(:director)
  end

  def item_type
    'Фильм'
  end

  def self.from_xml(xml)
    Film.new(name: xml.elements['name'].text,
             price: xml.elements['price'].text,
             quantity: xml.elements['quantity'].text,
             year: xml.elements['year'].text,
             director: xml.elements['director'].text)
  end

  def self.from_file(path)
    lines = File.readlines(path, chomp: true)
    Film.new(name: lines[0], price: lines[3], quantity: lines[4], year: lines[2], director: lines[1])
  end

  def to_s
    "#{item_type} \"#{@name}\", #{@year}, реж. #{@director}, #{@price} руб. Остаток: #{@quantity} шт."
  end
end
