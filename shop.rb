# frozen_string_literal: true

require_relative 'book'
require_relative 'film'
require_relative 'disk'
require_relative 'product_collection'

col = ProductCollection.new('')
col.read_xml('items.xml')
puts 'Что хотите купить?'
puts
col.collection.each { |e| puts "#{col.collection.index(e)}: #{e}" }
puts 'x. Покинуть магазин'
puts
choice = gets.chomp
price = 0
price = col.collection[choice.to_i].price if /^[0-9]*$/ === choice && (choice.to_i < col.collection.size)
puts "Спасибо за покупки, с Вас #{price} руб."
