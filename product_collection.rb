# frozen_string_literal: true

require 'rexml/document'

class ProductCollection
  attr_accessor :collection

  def to_a
    @collection
  end

  def initialize(*dir)
    @collection = []
    dir.each { |path| @collection += read_dir(path) } unless dir.empty?
  end

  def self.from_dir(dir)
    ProductCollection.new(dir)
  end

  def read_file(dir, file)
    if dir.include? 'books'
      Book.from_file(file.to_s)
    elsif dir.include? 'films'
      Film.from_file(file.to_s)
    end
  end

  def read_dir(dir)
    local_collection = []
    files = Dir["#{dir}*.txt"]
    files.each { |file| local_collection << read_file(dir, file) }
    local_collection
  end

  def read_xml(file_name)
    abort 'Извиняемся, хозяин, файлик не найден.' unless File.exist?("./data/#{file_name}")
    file = File.new("./data/#{file_name}")
    doc = REXML::Document.new(file)
    doc.elements.each('items/item') do |e|
      case e.attributes['type']
      when 'book' then @collection << Book.from_xml(e)
      when 'film' then @collection << Film.from_xml(e)
      when 'disk' then @collection << Disk.from_xml(e)
      end
    end
  end

  def sort!(var, reverse)
    # puts "@collection.sort_by!{ |obj| obj.#{var} }#{reverse ? '.reverse!' : ''}"
    # eval("@collection.sort_by!(&:#{var})#{reverse ? '.reverse' : ''}")
    eval("@collection.sort_by!{ |obj| obj.#{var} }#{reverse ? '.reverse!' : ''}")
  end

  def print
    @collection.each { |obj| puts obj.to_s }
  end
end
