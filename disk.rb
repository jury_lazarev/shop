# frozen_string_literal: true

require_relative 'item'

class Disk < Item
  attr_accessor :genre, :artist

  def initialize(params)
    super
    @genre = params[:genre].to_s
    @artist = params[:artist].to_s
  end

  def update(params)
    super
    @genre = params[:genre] if params.key?(:genre)
    @artist = params[:artist] if params.key?(:artist)
  end

  def self.from_file(path)
    lines = File.readlines(path, chomp: true)
    Disk.new(name: lines[0], price: lines[3], quantity: lines[4], genre: lines[1], artist: lines[2])
  end

  def self.from_xml(xml)
    Disk.new(name: xml.elements['name'].text,
             price: xml.elements['price'].text,
             quantity: xml.elements['quantity'].text,
             genre: xml.elements['genre'].text,
             artist: xml.elements['artist'].text)
  end

  def item_type
    'Диск'
  end

  def to_s
    "#{item_type} #{@artist} - #{@name} (#{@genre}), #{@price} руб. Остаток: #{@quantity} шт."
  end
end
