# frozen_string_literal: true

require_relative 'item'

class Book < Item
  attr_accessor :genre, :author

  def initialize(params)
    super
    @genre = params[:genre].to_s
    @author = params[:author].to_s
  end

  def update(params)
    super
    @genre = params[:genre] if params.key?(:genre)
    @author = params[:author] if params.key?(:author)
  end

  def self.from_file(path)
    lines = File.readlines(path, chomp: true)
    Book.new(name: lines[0], price: lines[3], quantity: lines[4], genre: lines[1], author: lines[2])
  end

  def self.from_xml(xml)
    Book.new(name: xml.elements['name'].text,
             price: xml.elements['price'].text,
             quantity: xml.elements['quantity'].text,
             genre: xml.elements['genre'].text,
             author: xml.elements['author'].text)
  end

  def item_type
    'Книга'
  end

  def to_s
    "#{item_type} \"#{@name}\", #{@genre}, автор - #{@author}, #{@price} руб. Остаток: #{@quantity} шт."
  end
end
